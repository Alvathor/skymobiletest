//
//  ErrorHandling.swift
//  FinancialApp
//
//  Created by Juliano Alvarenga on 15/08/18.
//  Copyright © 2018 Zion. All rights reserved.
//

import Foundation
import UIKit

extension Request {
    
    func errorHandling(error: Error?, resp: URLResponse?, activity: UIActivityIndicatorView) {
        guard error == nil else {
            let alert = UIAlertController.init(title: "Aviso", message: "falha ", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                DispatchQueue.main.async {
                    activity.stopAnimating()
                    print(error.debugDescription)
                }
            }))
            guard let rootController = UIApplication.shared.windows.first?.visibleViewController else { return }
            rootController.present(alert, animated: true, completion: nil)
            return
        }
        
        guard let statusCode = (resp as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
            let alert = UIAlertController.init(title: "Aviso", message: "falha ", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                DispatchQueue.main.async {
                    activity.stopAnimating()
                    print(error.debugDescription)
                }
            }))
            guard let rootController = UIApplication.shared.windows.first?.visibleViewController else { return }
            rootController.present(alert, animated: true, completion: nil)
            return
        }
    }
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: pvc)
            } else {
                return vc
            }
        }
    }
}
