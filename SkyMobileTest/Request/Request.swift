//
//  Request.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 24/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import Foundation
import UIKit

class Request: UIViewController {
    
    func buildURLNoQuery(path: BuildURL) -> URL {
        var urlComponent = URLComponents()
        urlComponent.scheme = BuildURL.sheme.rawValue
        urlComponent.host = BuildURL.host.rawValue
        urlComponent.path = path.rawValue
        
        return urlComponent.url!
    }
    
    //MARk: - Setup URLSession
    private var urlSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.allowsCellularAccess      = true
        configuration.httpShouldSetCookies      = true
        configuration.httpShouldUsePipelining   = true
        configuration.requestCachePolicy        = .useProtocolCachePolicy
        configuration.timeoutIntervalForRequest = 30.0
        configuration.urlCache = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        return  URLSession(configuration: configuration)
    }()       
    
    
    //MARK: - Defining Content
    fileprivate func defineContentType(method: MethodHTTP, url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        switch method {
        case .get:
            request.setValue(ContentType.get.rawValue, forHTTPHeaderField: "Content-Type")
        case .post:
            request.setValue(ContentType.postPutDel.rawValue, forHTTPHeaderField: "Content-Type")
            return request
        case .put:
            request.setValue(ContentType.postPutDel.rawValue, forHTTPHeaderField: "Content-Type")
            return request
        case .del:
            request.setValue(ContentType.postPutDel.rawValue, forHTTPHeaderField: "Content-Type")
            return request
        }
        
        return request
    }
    
    //MARK: - Generic Request
    func serviceAPI<T:Decodable>(method: MethodHTTP, url: URL, json: Data?, activityIndicator: UIActivityIndicatorView, completion: @escaping(T) -> ()) {
        var request = defineContentType(method: method, url: url)
        if let json = json {
            request.httpBody = json
        }
        let configuration   = URLSessionConfiguration.default
        let urlSession      = URLSession(configuration: configuration)
        urlSession.dataTask(with: request) { (data, resp, error) in
            self.errorHandling(error: error, resp: resp, activity: activityIndicator)
            do {
                guard let _data = data else { return }
                let obj         = try JSONDecoder().decode(T.self, from: _data)
                completion(obj)
            } catch {
                print(error)
            }
        }.resume()
    }        
    
    func requestPicture(urlString: URL, completion: @escaping (UIImage) -> ()) {        
        URLSession.shared.dataTask(with: urlString) { (data, resp, error) in
            if error != nil {
                return
            }
            guard let _data = data else { return }
            guard let image = UIImage(data: _data) else { return }
            completion(image)
        }.resume()
    }
}

enum MethodHTTP: String {
    case get    = "GET"
    case post   = "POST"
    case put    = "PUT"
    case del    = "DELETE"
}

enum ContentType: String {
    case get        = "application/x-www-form-urlencoded"
    case postPutDel = "application/json"
}

enum BuildURL: String {
    case sheme  = "https"
    case host   = "sky-exercise.herokuapp.com"
    case movies = "/api/movies"
}
