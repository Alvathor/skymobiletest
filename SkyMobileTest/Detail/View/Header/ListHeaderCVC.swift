//
//  ListHeaderCVC.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 25/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

class ListHeaderCVC: UICollectionReusableView {
    
    @IBOutlet weak var backGroundImage: UIImageView!
    
    var animator: UIViewPropertyAnimator!
    
    override func awakeFromNib() {        
        setupHeader()
        setupBlurEffect()
    }
    
    func setupHeader() {
        backGroundImage.image = UIImage(named: "redBackgroundSky")
        backGroundImage.alpha = 0
    }
    
    fileprivate func setupBlurEffect() {
        
        animator = UIViewPropertyAnimator(duration: 3, curve: .linear, animations: { [weak self] in
            let blur = UIVisualEffectView()
            blur.effect = UIBlurEffect(style: .dark)
            blur.alpha = 0.5
            self?.addSubview(blur)            
            self?.fillSuperview(view: blur)
            self?.backGroundImage.alpha = 1
        })
    }
    
    fileprivate func fillSuperview(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        let _trailingAnchor = view.trailingAnchor.constraint(equalTo: trailingAnchor)
        let _leadingAnchor = view.leadingAnchor.constraint(equalTo: leadingAnchor)
        let _topAnchor = view.topAnchor.constraint(equalTo: topAnchor)
        let _bottomAnchor = view.bottomAnchor.constraint(equalTo: bottomAnchor)
        
        NSLayoutConstraint.activate([_trailingAnchor, _leadingAnchor, _topAnchor, _bottomAnchor])
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
