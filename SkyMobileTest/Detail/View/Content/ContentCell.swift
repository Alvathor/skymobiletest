//
//  ContentCell.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 25/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

class ContentCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    func setupView(movie: ListModel) {
        title.text = movie.title
        duration.text = movie.duration
        year.text = movie.release_year
        textView.text = movie.overview
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String  {
        return String(describing: self)
    }
}
