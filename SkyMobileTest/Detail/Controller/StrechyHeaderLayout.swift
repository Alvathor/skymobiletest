//
//  StrechyHeaderLayout.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 26/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

class StrechyHeaderLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect)
        
        layoutAttributes?.forEach({ (attribute) in
            if attribute.representedElementKind == UICollectionView.elementKindSectionHeader && attribute.indexPath.section == 0 {
                
                let widht = collectionView?.frame.width ?? 0.0
                let contentOffSetY = collectionView?.contentOffset.y ?? 0.0
                let height = attribute.frame.height - contentOffSetY
                
                if contentOffSetY > 0 {
                    return
                }
                
                attribute.frame = CGRect(x: 0, y: contentOffSetY, width: widht, height: height)
            }
        })
        
        return layoutAttributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
