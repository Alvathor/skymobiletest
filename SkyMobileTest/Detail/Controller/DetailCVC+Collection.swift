//
//  DetailCVC+Collection.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 26/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

extension DetailCVC: UICollectionViewDelegateFlowLayout {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffSetY = scrollView.contentOffset.y        
        if contentOffSetY > 0 {
            header.animator.fractionComplete = 0
            return
        }
        header.animator.fractionComplete = abs(contentOffSetY) / 100
        if header.animator.fractionComplete == 1 {
            header.animator.stopAnimation(true)
            self.dismiss(animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {        
        return CGSize(width: view.frame.width, height: view.frame.height / 1.8 )
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ListHeaderCVC.identifier, for: indexPath) as? ListHeaderCVC
        
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ContentCell.identifier, for: indexPath) as! ContentCell
        let height = view.frame.height - view.frame.height / 1.8
        cell.frame.size.width = view.frame.width
        cell.frame.size.height = height        
        cell.setupView(movie: movie)
        
        return cell
    }
    
    
}
