//
//  DetailCVC.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 25/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

class DetailCVC: UICollectionViewController {

    var movie: ListModel!
    var backgoundImage: UIImage!
    var header: ListHeaderCVC!
    let photo = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.photo.frame = self.view.frame
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func setupAll() {
        setupCollectionView()
        setupBackgroundPhoto()
        setupShadowView()
    }
    
    fileprivate func setupBackgroundPhoto() {
        photo.contentMode = .scaleAspectFill
        photo.image = backgoundImage
        photo.frame = CGRect(x: -backgoundImage.size.width / 2, y: 0, width: backgoundImage.size.width, height: backgoundImage.size.height)
        
        view.insertSubview(photo, belowSubview: collectionView)
    }
    
    fileprivate func setupShadowView() {
        let shadowLayer = gradientLayer(holderView: view)
        let shadowView = UIView()
        shadowView.frame = view.frame
        shadowView.layer.insertSublayer(shadowLayer, at: 0)
        
        view.insertSubview(shadowView, aboveSubview: photo)
    }
    
    fileprivate func gradientLayer(holderView: UIView) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        let top = UIColor.clear.cgColor
        let bottom = UIColor.black.withAlphaComponent(0.98).cgColor
        gradientLayer.colors = [top, bottom]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.6)
        gradientLayer.frame.size.width = holderView.frame.width
        gradientLayer.frame.size.height = holderView.frame.height
        
        return gradientLayer
    }
    
    fileprivate func setupCollectionView() {
        collectionView.backgroundColor = .clear
        collectionView.collectionViewLayout = StrechyHeaderLayout()
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.register(ListHeaderCVC.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ListHeaderCVC.identifier)
        collectionView.register(ContentCell.nib, forCellWithReuseIdentifier: ContentCell.identifier)
    }
    
}


