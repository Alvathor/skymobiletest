//
//  ListNetworking.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 24/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

extension ListCVC {
 
    func getList() {
        activityIndicator.startAnimating()
        let url = request.buildURLNoQuery(path: .movies)        
        request.serviceAPI(method: .get, url: url, json: nil, activityIndicator: activityIndicator) { [weak self] (resp: [ListModel]) in
            self?.listOfMovies = resp
            self?.isRequestDone = true
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.activityIndicator.stopAnimating()
                self?.refreshControl.endRefreshing()
            }
        }
    }
}
