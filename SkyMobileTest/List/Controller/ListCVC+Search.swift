//
//  ListCVC+Search.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 25/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

extension ListCVC: UISearchBarDelegate, UISearchControllerDelegate {
    
    func setupSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Buscar"
        searchController.searchBar.tintColor = UIColor(named: "redDetails")
        searchController.searchBar.barTintColor = UIColor(named: "redDetails")
        searchController.extendedLayoutIncludesOpaqueBars = true
        searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            guard let backgroundview = textfield.subviews.first else { return }
            backgroundview.layer.cornerRadius = 10
            backgroundview.clipsToBounds = true
            backgroundview.backgroundColor = .black
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            isSearching = false
        } else {
            isSearching = true
        }
        
        let filter = listOfMovies.filter({$0.title?.localizedStandardContains(searchText) ?? false})
        filteredMovies = filter
        collectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        collectionView.reloadData()        
    }
}
