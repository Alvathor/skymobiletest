//
//  ListCVC+Table.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 25/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

extension ListCVC: UICollectionViewDelegateFlowLayout {
    
    fileprivate func returnList() -> [ListModel] {
        var list = [ListModel]()
        if isRequestDone {
            list = isSearching ? filteredMovies : listOfMovies
        }
        return list
    }
    
    //MARK: - N A V I G A T I O N
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? DetailCVC else { return }
        destination.movie = returnList()[index]
        destination.backgoundImage = backgroundImage
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return returnList().count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.identifier, for: indexPath) as! MovieCell
        cell.setupCell(movie: returnList()[indexPath.row])
        cell.getCoverImage(fromThisUrl: returnList()[indexPath.row].cover_url ?? "")
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? MovieCell else { return }
        index = indexPath.row
        backgroundImage = cell.coverImage.image
        
        performSegue(withIdentifier: "toDetail", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        let numberColum = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = collectionView.contentInset.left + collectionView.contentInset.right + (flowLayout.minimumLineSpacing * CGFloat(numberColum - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberColum))
        
        return CGSize(width: Double(size), height: Double(size) * 1.7)
    }
}
