//
//  ListCVC.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 24/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

class ListCVC: UICollectionViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var request = Request()
    var refreshControl = UIRefreshControl()
    var listOfMovies: [ListModel]!
    var filteredMovies: [ListModel]!
    var index: Int!
    var backgroundImage: UIImage!
    var isRequestDone = false
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAll()        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func setupAll() {
        SetupRefreshControll()
        SetupCollectionView()
        setupSearchBar()
        getList()
    }
        
    fileprivate func SetupRefreshControll() {
        refreshControl.addTarget(self, action: #selector(refreshTable(_:)), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        refreshControl.tintColor = UIColor(named: "redDetails")
    }
    
    @objc fileprivate func refreshTable(_ sender: UIRefreshControl ) {
        getList()
    }       
    
    fileprivate func SetupCollectionView() {        
        collectionView.register(MovieCell.nib, forCellWithReuseIdentifier: MovieCell.identifier)
        collectionView.backgroundColor = UIColor(named: "blackBackground")
    }        
}


