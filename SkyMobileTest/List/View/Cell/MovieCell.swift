//
//  MovieCell.swift
//  SkyMobileTest
//
//  Created by Juliano Alvarenga on 24/01/19.
//  Copyright © 2019 Juliano Alvarenga. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {

    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var request = Request()
    let imageCache = NSCache<NSString, UIImage>()
    
    override func awakeFromNib() {
        coverView.layer.cornerRadius = 15
        coverView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        titleLabel.text?.removeAll()
        coverImage.image = nil
    }

    func setupCell(movie: ListModel) {
        titleLabel.text = movie.title
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    fileprivate func setupImage(image: UIImage) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                self.coverImage.image = image
                self.coverImage.alpha = 1
            })
        }
    }
    
    func getCoverImage(fromThisUrl: String) {
        guard let url = URL(string: fromThisUrl) else { return }
        let urlFromCache = url.absoluteString
        if let imageFromCache = imageCache.object(forKey: url.absoluteString as NSString) {
            setupImage(image: imageFromCache)
            return
        } else {
            request.requestPicture(urlString: url) { (image) in
                if urlFromCache == url.absoluteString {
                    self.setupImage(image: image)
                    self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                }
            }
        }
    }
}
